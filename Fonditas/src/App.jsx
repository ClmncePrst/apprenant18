import Favoritas from './components/Favoritas'
import FondasCercanas from './components/FondasCercanas'
import MejoresMenus from './components/MejoresMenus'
import Header from './components/Header'
import './App.css'
import CategoryTitles from './components/CategoryTitles'

function App() {


  return (
    <>
    <div>
      <Header/>
    </div>
      <CategoryTitles titre="Las Favoritas" />
      <div className='favoritasTotal'>

        < Favoritas nom="Pizzas"
          desde="$60"
          img="../pictures/fonditas-main/category-pizza.png"
        />

        < Favoritas nom="Sushi"
          desde="$50"
          img="../pictures/fonditas-main/category-sushi.png"
        />

        < Favoritas nom="Hamburgesas"
          desde="$50"
          img="../pictures/fonditas-main/category-hamburger.png"
        />

        < Favoritas nom="Veggie"
          desde="$50"
          img="../pictures/fonditas-main/category-veggie.png"
        />

        < Favoritas nom="Sopas"
          desde="$50"
          img="../pictures/fonditas-main/category-soup.png"
        />

        < Favoritas nom="Postres"
          desde="$50"
          img="../pictures/fonditas-main/category-dessert.png" />
      </div>

      <CategoryTitles titre="Fondas cercanas" />
      <div className='fondasCercanasTotal'>
        <FondasCercanas nom="Le cottidiene"
          img="../pictures/fonditas-main/dish-cottidiene.jpeg"
          time="15-20 min"
        />

        < FondasCercanas nom="Dona Laura"
          img="../pictures/fonditas-main/dish-dona-laura.jpeg"
          time="20-30 min"
        />

        <FondasCercanas nom="Querreque"
          img="../pictures/fonditas-main/dish-querreque.jpeg"
          time="~50 min"
        />

        <FondasCercanas nom="Rosa Cafe"
          img="../pictures/fonditas-main/dish-rosa-cafe.jpeg"
          time="~45 min" />

      </div>
      <CategoryTitles titre="Los mejores menus" />
      <div className='losMejoresMenusTotal'>
        <MejoresMenus nom="Hot caketerías"
          img="../pictures/fonditas-main/menu-hot-caketerias.jpeg"
          time="20-30 min"
          desde="$70"
        />

        <MejoresMenus nom="Pizzeria"
          img="../pictures/fonditas-main/menu-pizzeria.jpeg"
          time="~45 min"
          desde="$70"
        />

        <MejoresMenus nom="Desayunos"
          img="../pictures/fonditas-main/menu-desayunos.jpeg"
          time="15-20 min"
          desde="$50"
        />

        <MejoresMenus nom="Ensaladish"
          img="../pictures/fonditas-main/menu-ensaladish.jpeg"
          time="30-40 min"
          desde="$70"
        />

        <MejoresMenus nom="Otros Desayunos"
          img="../pictures/fonditas-main/menu-otros-desayunos.jpeg"
          time="~20 min"
          desde="$50"
        />
        <MejoresMenus nom="Pastish"
          img="../pictures/fonditas-main/menu-pastish.jpeg"
          time="10-20 min"
          desde="$50"
        />


      </div>
    </>
  )
}

export default App
