import StarButton from "./StarButton"
import "./MejoresMenus.css"

function MejoresMenus(props) {

    return (
        <div className="mejoresMenu">
            <div>
                <img src={props.img} alt="categoryIcon" />
            </div>

            <div className="nameMejores">
                {props.nom}
            </div>

            <div className="desdeMejores">
                Desde {props.desde}
            </div>
            <div className="timeMejores">
                Time {props.time}
            </div>
            < StarButton />
        </div>
    )
}
export default MejoresMenus