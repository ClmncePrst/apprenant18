import "./Header.css";

function Header() {
  return (
    <header>
      <div className="Header-container">
        <h1>Fonditas</h1>

        <nav>
          <div >
            <ul className="main-menu">
              <li>
                <a href="">Nosotros</a>
              </li>
              <li>
                <a href="">Fonditas</a>
              </li>
              <li>
                <a href="">Mapa</a>
              </li>
              <li>
                <a href="">Inscribirse</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <h2>De tu fonda favorite</h2>
      <h3>La comida que ya conoces al mejor precio</h3>
    </header>
  );
}

export default Header;
