import "./Favoritas.css"

function Favoritas(props) {

    return (
        <div className="favoritas">
            <div className="iconeFavoritas">
                <img src={props.img} alt="categoryIcon" />
            </div>

            <div className="FavoritasName">
                {props.nom}
            </div>

            <div className="DesdeFavoritas">
                Desde {props.desde}
            </div>


        </div>
    )
}

export default Favoritas