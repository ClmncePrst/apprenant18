import StarButton from "./StarButton";
import "./FondasCercanas.css"


function FondasCercanas(props) {
    return (

        <div className="fondasCercanas">
            <div className="fondasCercanasImage">
                <img src={props.img} alt="dishImage" />
            </div>

            <div className="fondasCercanasName">
                {props.nom}
            </div>

            <div className="fondasCercanasTime">
                Time {props.time}
            </div>
            < StarButton />

        </div>
    )
}

export default FondasCercanas;