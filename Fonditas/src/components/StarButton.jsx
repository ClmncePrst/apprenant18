import "./StarButton.css"

function StarButton() {
    return (
        <button type="button" className="StarButton">
            <img src="../public/etoile-favoris.png" alt="" /> 9.8
        </button>
    )
};


export default StarButton;
